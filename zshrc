# ZSH config
autoload -Uz compinit promptinit

#   up arrow history
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

compinit
promptinit

function virtualenv_info {
    if [[ $VIRTUAL_ENV ]] then
        echo '('`basename $VIRTUAL_ENV`')'
    fi
}

function check_last_exit_code() {
    local LAST_EXIT_CODE=$?
    if [[ $LAST_EXIT_CODE -ne 0 ]]; then
        echo "  (ec:$LAST_EXIT_CODE)"
    fi
}
RPROMPT='$(check_last_exit_code)$(virtualenv_info)'

#   Current zsh THEME:
#   /usr/share/zsh/functions/Prompts/prompt_adam2_setup

#   DIFF for zsh THEME
#  virtues=("φρόνησις, prudentia" "δικαιοσύνη,iustitia" "ἀνδρεία,fortitudo" "σωφροσύνη, temperantia")
#  prompt_char="$virtues[RANDOM % $#virtues+1];"p

# This will set the default prompt to the adam2 theme
prompt adam2 8bit 223 cyan green 223

# ZSH command completion
zstyle ':completion:*' menu select
setopt COMPLETE_ALIASES

#   Key bindings
#   create zkbd compatible hash;
#   details: man 5 terminfo
typeset -g -A key
key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"

[[ -n "${key[Home]}"    ]] && bindkey -- "${key[Home]}" beginning-of-line
[[ -n "${key[End]}"    ]] && bindkey -- "${key[End]}" end-of-line
[[ -n "${key[Insert]}"    ]] && bindkey -- "${key[Insert]}" overwrite-mode
[[ -n "${key[Backspace]}"    ]] && bindkey -- "${key[Backspace]}" backward-delete-char
[[ -n "${key[Delete]}"    ]] && bindkey -- "${key[Delete]}" delete-char
[[ -n "${key[Up]}"    ]] && bindkey -- "${key[Up]}" up-line-or-beginning-search
[[ -n "${key[Down]}"    ]] && bindkey -- "${key[Down]}" down-line-or-beginning-search
[[ -n "${key[Left]}"    ]] && bindkey -- "${key[Left]}" backward-char
[[ -n "${key[Right]}"    ]] && bindkey -- "${key[Right]}" forward-char
[[ -n "${key[PageUp]}"    ]] && bindkey -- "${key[PageUp]}" beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"    ]] && bindkey -- "${key[PageDown]}" end-of-buffer-or-history
[[ -n "${key[Shift-Tab]}"    ]] && bindkey -- "${key[Shift-Tab]}" reverse-menu-complete

#   Finally, make sure the terminal is in application mode, when
#   zle is active. Only then are the values from $terminfo valid.
#
if  ((  ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
    autoload -Uz add-zle-hook-widget
    function zle_application_mode_start { echoti smkx }
    function zle_application_mode_stop { echoti rmkx }
    add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
    add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi

# Kappa zone
alias vim='/usr/bin/nvim'
alias df='df -h'
alias _ggwp='shutdown now'
alias ll='ls -lah'
alias l='ls -lah --color'
alias reap='cd ~/git/reaping'
alias mkdir='mkdir -p'
alias cdg='cd ~/git/yacto'
alias wegood='sensors'
alias weather='curl "wttr.in"'
export EDITOR=/usr/bin/nvim
export PATH=/home/stoic/.local/bin:/usr/bin:$PATH
export BROWSER=/usr/bin/firefox
export HISTSIZE=10000
export SAVEHIST=10000
export HISTFILE=~/.zsh_history
setopt EXTENDED_HISTORY

fg()
{
    if [[ "$1" = <-> ]] then
        builtin fg "%$@"
    else
        builtin fg $@
    fi
}

bg()
{
    if [[ "$1" = <-> ]] then
        builtin bg "%$@"
    else
        builtin bg $@
    fi
}




function __fuck_vpn()
{
    while [ true ]; do sudo systemctl restart vpnc@cinemo; if [ $? -eq 0 ]; then echo "done"; break; fi; done
}
