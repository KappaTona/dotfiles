set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
set guicursor=
let g:pymode_options_colorcolumn = 1
set noincsearch
"   disable python2
let g:loaded_python_provider = 0
source ~/.vimrc
hi ColorColumn ctermbg=223
