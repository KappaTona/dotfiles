"#execute pathogen#infect()
syntax on
filetype plugin indent on
set nocompatible
set hidden
set wildmenu
set showcmd
set hlsearch
set autoindent
set nostartofline
set laststatus=2
set confirm
set visualbell
set t_vb=
set mouse=a
set cmdheight=2
set number
set shiftwidth=4
set softtabstop=4
set tabstop=4
set expandtab
"indentation step
set sw=4
" colorscheme molokai
colorscheme herokudoc
hi Normal guibg=NONE ctermbg=NONE
hi NonText guibg=NONE ctermbg=NONE


"let g:ycm_global_ycm_extra_conf = '~/.vim/.ycm_extra_conf.py'
"let g:ycm_global_ycm_extra_conf = '/usr/share/vim/vimfiles/third_party/ycmd/cpp/ycm/.ycm_extra_conf.py'
let g:ycm_confirm_extra_conf = 0
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_server_python_interpreter = '/usr/bin/python'
"set pythonthreehome=
"set pythonthreedll=
"let g:ycm_python_binary_path = 'python'

set wildignore=""
set backspace=2


hi SpecialKey ctermbg=red guibg=red

let g:templates_no_autocmd = 1

"kappa
set statusline=%F%m%r%h%w\ [POS=%04l,%04v][%p%%]
set ff=unix
map <F5> :noh <CR>
map <F6> :set t_Co=0 <CR>

" light version
"tmp switch of trailing white spaces
set list
"set listchars=tab:\ \ ,trail:\ 
set listchars=tab:\ \ ,trail:\ 

" Plugin handling stuffs 
" https://github.com/VundleVim/Vundle.vim <-- details
" Keep Plugin commands between vundle#begin/end.
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'python-mode/python-mode'
Plugin 'mileszs/ack.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'pseewald/vim-anyfold'
Plugin 'arecarn/vim-fold-cycle'
"Plugin 'levelone/tequila-sunrise.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

"ack.vim config
if executable('ag')
    let g:ackgrp = 'ag --vimgrep'
endif

" vim-anyfold
autocmd Filetype cpp,h AnyFoldActivate
set foldignore=/*
set foldlevel=0     "close all folds
let g:anyfold_fold_comments=1

" vim-cycle-fold
nmap <Tab><Tab> <Plug>(fold-cycle-open)
nmap <S-Tab><S-Tab> <Plug>(fold-cycle-close)

"   YCM bohocsag -- install.py result ; tail -n 5
"Cached Clangd archive does not match checksum. Removing...
"Downloading Clangd from https://dl.bintray.com/ycm-core/clangd/clangd-11.0.0-x86_64-unknown-linux-gnu.tar.bz2...
"Extracting Clangd to /home/stoic/.vim/bundle/YouCompleteMe/third_party/ycmd/third_party/clangd/output...
"Done installing Clangd
"Clangd completer enabled. If you are using .ycm_extra_conf.py files, make sure they use Settings() instead of the old and deprecated FlagsForFile()


" YCM switch on - off TODO
"After you toggle the value of g:ycm_show_diagnostics_ui, run :YcmRestartServer and then reload the file with :e.
"
"let g:ycm_show_diagnostics_ui = 0

"set tags=/home/kappa/git/dom/tags
