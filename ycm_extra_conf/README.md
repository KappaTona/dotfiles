As of 2021 dec 3 I updated the ycm vim bundle.
issue `python install.py --clangd-completer`
by default no `.ycm_extra_conf.py` is needed if there is a `compile_commands.json` file in
`${CMAKE_SOURCE_DIR}.` So far `ln -s _build/compile_commands.json .` works

---
The Solution!!!

//source:https://www.reddit.com/r/vim/comments/2ez19c/an_allaround_solution_for_ycms_ycm_extra_confpy/ck4h9eb/

// with clang

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

${CMAKE_BINARY_DIR}/compile_commands.json

Pate .ycm_extra_conf.py in the top of your project directory (top level of CMakeLists.txt)
